# Guild Wars Client Bot API

## 1. Building the gwcba static library

## 1.1 Building prerequisites
- Visual Studio 2019 (Enterprise / Professional / [Community (Free)](https://www.visualstudio.com/vs/community/))
    - Desktop development with C++

If you don't use Jetbrains [CLion IDE](https://www.jetbrains.com/clion/), you need to install CMake on your own :
- [CMake 3.19 or higher](https://cmake.org/download/)

- Clone the project :
```sh 
$ git clone https://gitlab.com/QuarkyUp/gw-client-bot-api.git
```

- Navigate to the project folder:
```sh
  $ cd gw-client-bot-api
```

## Building the project
### Using command line
```sh 
    - 'cmake . -DCMAKE_BUILD_TYPE=RelWithDebInfo -A Win32'
    - 'cmake --build . --config Release --target gwcba'
```

The last command create a new `Release` folder with the library `gwcba.lib` library file inside.

# Credits
## Creators of [GWCA](https://github.com/GregLando113/GWCA)
