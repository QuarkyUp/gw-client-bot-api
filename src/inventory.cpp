#include <gwcba/agents.h>
#include <gwcba/inventory.h>
#include <gwcba/merchant.h>
#include <gwcba/utils.h>

bool blockBeforeSalvage = false;
bool blockAfterSalvage = false;
bool blockRuneSell = true;

std::vector<GW::Item *> getInventoryBagsItems() {
  std::vector<GW::Item *> inventoryBagsItems;

  const auto bags = GW::Items::GetBagArray();

  if (bags == nullptr) return inventoryBagsItems;

  for (auto bagIndex = static_cast<size_t>(GW::Constants::Bag::Backpack); bagIndex <= static_cast<size_t>(GW::Constants::Bag::Bag_2); ++bagIndex) {
    auto bag = GW::Items::GetBag(bagIndex);

    if (!bag) continue;
    if (!bag->items.valid()) continue;

    for (const auto &bagItem : bag->items)
      if (!bagItem) inventoryBagsItems.push_back(bagItem);
  }

  return inventoryBagsItems;
}

uint32_t getInventoryTotalSlotsCount() {
  uint32_t inventorySlotsCount = 0;

  for (auto bagIndex = static_cast<size_t>(GW::Constants::Bag::Backpack); bagIndex < static_cast<size_t>(GW::Constants::Bag::Bag_2); ++bagIndex) {
    const auto inventoryBag = GW::Items::GetBag(bagIndex);
    if (!inventoryBag) continue;
    inventorySlotsCount += inventoryBag->items.m_size;
  }

  return inventorySlotsCount;
}

uint32_t getInventoryAvailableSlotsCount() { return getInventoryTotalSlotsCount() - getInventoryBagsItems().size(); }

const GW::Item *getItemFromInventory(uint32_t itemModelId) {
  const auto inventoryBagsItems = getInventoryBagsItems();

  for (const auto &item : inventoryBagsItems)
    if (item->model_id == itemModelId) return item;

  return nullptr;
}

const GW::Item *getSuperiorIdentificationKitFromInventory() { return getItemFromInventory(5899); }

const GW::Item *getSuperiorSalvageKitFromInventory() { return getItemFromInventory(5900); }

bool defaultCanPickup(const GW::Item *item) {
  if (!item) return false;

  if (item->GetIsMaterial()) return true;

  if (item->model_id == 22190) return true;                                                         // shamrock ale
  if (item->model_id == 22191) return true;                                                         // four leaf clover
  if (item->model_id == 819) return true;                                                           // dragon roots
  if (item->model_id == 22751) return true;                                                         // lockpicks
  if (item->model_id == 2511 && GW::Items::GetGoldAmountOnCharacter() <= item->value) return true;  // gold

  return false;
}

bool pickupItem(const GW::Item *item, const std::function<bool(const GW::Item *itemToPickUp)> &canPickup) {
  if (!canPickup(item)) return false;
  GW::CtoS::SendPacket(0xC, GAME_CMSG_INTERACT_ITEM, item->agent_id, 0);

  return true;
}

bool pickupItemsInRangeOfPlayer(float range, const std::function<bool(const GW::Item *itemToPickUp)> &canPickup) {
  const auto &agents = *GW::Agents::GetAgentArray();
  const auto player = GW::Agents::GetPlayer();

  if (!agents.valid()) return false;
  if (!arePointersValid()) return false;

  const auto beginLootTimer = std::chrono::high_resolution_clock::now();

  const auto agentItemsInRange = getAgentsItemInRangeOfAgent(range);

  for (const auto &agentItem : agentItemsInRange) {
    if (!arePointersValid()) continue;

    const auto item = GW::Items::GetItemById(agentItem->item_id);
    if (pickupItem(item, canPickup)) {
      const auto begin = std::chrono::high_resolution_clock::now();

      while (true) {
        Sleep(50);
        if (!player) break;
        if (!player->GetAsAgentLiving()) break;
        if (player->GetAsAgentLiving()->GetIsDead()) break;

        if (!GW::Agents::GetAgentByID(agentItem->agent_id)) {
          Sleep(250);
          break;
        }

        const auto end = std::chrono::high_resolution_clock::now();
        const auto endLootTimer = std::chrono::high_resolution_clock::now();

        const auto timediffMs = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
        const auto timediffMsLoot = std::chrono::duration_cast<std::chrono::milliseconds>(endLootTimer - beginLootTimer).count();

        if (timediffMs > 5000) break;
        if (timediffMsLoot > 60000) return false;
      }
    }
  }

  return true;
}

bool identify(const GW::Item *item, const GW::Item *kit) {
  if (!item || !kit) return false;
  if ((item->interaction & 1) != 0) return false;  // already identified
  GW::CtoS::SendPacket(0xC, GAME_CMSG_ITEM_IDENTIFY, kit->item_id, item->item_id);

  return true;
}

bool identifyInventoryItems() {
  const auto bagsItems = getInventoryBagsItems();

  for (const auto &bagItem : bagsItems) {
    if ((bagItem->interaction & 1) != 0) continue;  // already identified
    if (isItemARune(bagItem)) continue;

    if (!getSuperiorIdentificationKitFromInventory())
      if (!buySuperiorIdentificationKit()) return false;

    if (!identify(bagItem, getSuperiorIdentificationKitFromInventory())) continue;

    Sleep(500);
  }

  return true;
}

void reverseByPairs(std::string &s) {
  if (s.size() & 1) return;

  std::reverse(begin(s), end(s));

  for (size_t i = 0; i + 1 < s.size(); i += 2) {
    std::swap(s[i], s[i + 1]);
  }
}

bool doesModstringContainsRune(const std::string &modstring) {
  std::vector<std::string> runeModstrings = {
      "012BE821",     "012CE821",     "0129E821",     "012AE821",     "022BE8210703", "022CE8210703", "0229E8210703", "022AE8210703", "032BE8210903",
      "032CE8210903", "0329E8210903", "032AE8210903", "0126E821",     "0128E821",     "0127E821",     "0125E821",     "0226E8210D03", "0228E8210D03",
      "0227E8210D03", "0225E8210D03", "0326E8210F03", "0328E8210F03", "0327E8210F03", "0325E8210F03", "0102E821",     "0100E821",     "0101E821",
      "0103E821",     "0202E8216B01", "0200E8216B01", "0201E8216B01", "0203E8216B01", "0302E8217701", "0300E8217701", "0301E8217701", "0303E8217701",
      "0104E821",     "0107E821",     "0105E821",     "0106E821",     "0204E8216D01", "0207E8216D01", "0205E8216D01", "0206E8216D01", "0304E8217901",
      "0307E8217901", "0305E8217901", "0306E8217901", "0108E821",     "0109E821",     "010CE821",     "010BE821",     "010AE821",     "0208E8216F01",
      "0209E8216F01", "020CE8216F01", "020AE8216F01", "020BE8216F01", "0308E8217B01", "0309E8217B01", "030CE8217B01", "030AE8217B01", "030BE8217B01",
      "0110E821",     "010DE821",     "010FE821",     "010EE821",     "020DE8217101", "020FE8217101", "020EE8217101", "0210E8217101", "0310E8217D01",
      "030DE8217D01", "030FE8217D01", "030EE8217D01", "EA02E827",     "0112E821",     "0113E821",     "0111E821",     "0114E821",     "0115E821",
      "EA02E927",     "0212E8217301", "0213E8217301", "0211E8217301", "0214E8217301", "0215E8217301", "EA02EA27",     "0312E8217F01", "0313E8217F01",
      "0311E8217F01", "0314E8217F01", "0315E8217F01", "0116E821",     "0117E821",     "0119E821",     "0216E8217501", "0217E8217501", "0219E8217501",
      "0218E8217501", "0316E8218101", "0317E8218101", "0319E8218101", "0319E8218101", "0318E8218101", "0123E821",     "011DE821",     "011EE821",
      "011FE821",     "0223E8217902", "021DE8217902", "021EE8217902", "021FE8217902", "0323E8217B02", "031DE8217B02", "031EE8217B02", "031FE8217B02",
      "0122E821",     "0120E821",     "0121E821",     "0124E821",     "0222E8217F02", "0220E8217F02", "0221E8217F02", "0224E8217F02", "0322E8218102",
      "0320E8218102", "0321E8218102", "0324E8218102", "01087827",     "C202E927",     "05067827",     "07047827",     "00037827",     "C202EA27",
      "0200D822",     "C202E827",     "000A4823"};

  for (const auto &runestring : runeModstrings)
    if (modstring.find(runestring) != std::string::npos) return true;

  return false;
}

bool doesModstringContainsInsignia(const std::string &modstring) {
  std::vector<std::string> insigniaModstrings = {
      "DE010824", "DF010824", "E0010824", "E1010824", "E2010824", "E3010824", "E4010824", "E5010824", "E6010824", "E7010824", "E8010824", "E9010824",
      "EA010824", "EB010824", "0A020824", "EC010824", "ED010824", "EE010824", "EF010824", "F0010824", "F1010824", "F2010824", "F3010824", "F4010824",
      "F5010824", "F6010824", "F7010824", "F8010824", "F9010824", "08020824", "09020824", "FA010824", "FB010824", "FC010824", "FD010824", "FE010824",
      "FF010824", "00020824", "01020824", "02020824", "03020824", "04020824", "05020824", "06020824", "07020824"};

  for (const auto &insigniaString : insigniaModstrings)
    if (modstring.find(insigniaString) != std::string::npos) return true;

  return false;
}

void defaultSalvageSessionPacketCallback(GW::HookStatus *hookStatus, [[maybe_unused]] GW::Packet::StoC::SalvageSession *pak) {
  if (hookStatus) hookStatus->blocked = true;
  blockBeforeSalvage = false;
  blockAfterSalvage = false;
}

void defaultSalvageSessionDonePacketCallback(GW::HookStatus *hookStatus, [[maybe_unused]] GW::Packet::StoC::SalvageSessionDone *pak) {
  if (hookStatus) hookStatus->blocked = true;
  blockBeforeSalvage = false;
  blockAfterSalvage = false;
}

void defaultSalvageSessionSuccessPacketCallback(GW::HookStatus *hookStatus, [[maybe_unused]] GW::Packet::StoC::SalvageSessionSuccess *pak) {
  if (hookStatus) hookStatus->blocked = true;
  GW::CtoS::SendPacket(0x4, GAME_CMSG_ITEM_SALVAGE_SESSION_DONE);
  blockBeforeSalvage = false;
  blockAfterSalvage = false;
}

void defaultSalvageSessionItemKeptPacketCallback(GW::HookStatus *hookStatus, [[maybe_unused]] GW::Packet::StoC::SalvageSessionItemKept *pak) {
  if (hookStatus) hookStatus->blocked = true;
  GW::CtoS::SendPacket(0x4, GAME_CMSG_ITEM_SALVAGE_SESSION_DONE);
  blockBeforeSalvage = false;
  blockAfterSalvage = false;
}

std::string getModstringFromItem(const GW::Item *item) {
  std::string modstringfull;
  for (size_t i = 0; i < item->mod_struct_size; i++) {
    std::stringstream stream;
    stream << std::hex << item->mod_struct[i].mod;
    std::string result(stream.str());
    reverseByPairs(result);
    modstringfull += result;
  }
  std::transform(modstringfull.begin(), modstringfull.end(), modstringfull.begin(), [](int c) -> char { return static_cast<char>(::toupper(c)); });

  return modstringfull;
}

void salvageMod(const GW::Item *item, uint32_t modType) {
  if (!getSuperiorSalvageKitFromInventory()) buySalvageKitLogic();

  const auto salvageKit = getSuperiorSalvageKitFromInventory();
  if (!salvageKit) return;

  blockBeforeSalvage = true;
  GW::CtoS::SendPacket(0x10, GAME_CMSG_ITEM_SALVAGE_SESSION_OPEN, GW::GameContext::instance()->world->salvage_session_id, salvageKit->item_id,
                       item->item_id);

  while (blockBeforeSalvage) Sleep(50);

  blockAfterSalvage = true;
  GW::CtoS::SendPacket(0x8, GAME_CMSG_ITEM_SALVAGE_UPGRADE, modType);
  while (blockAfterSalvage) Sleep(50);

  while (true) {
    Sleep(50);
    const std::string modstringfullAfterSalvage = getModstringFromItem(item);
    if (!doesModstringContainsRune(modstringfullAfterSalvage) && !blockAfterSalvage) break;
  }
}

void salvageRune(const GW::Item *item) { salvageMod(item, 1); }

void salvageInsignia(const GW::Item *item) { salvageMod(item, 0); }

void salvageModsFromInventoryItems() {
  GW::HookEntry salvageHookEntry;

  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::SalvageSession>(&salvageHookEntry, defaultSalvageSessionPacketCallback);
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::SalvageSessionDone>(&salvageHookEntry, defaultSalvageSessionDonePacketCallback);
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::SalvageSessionSuccess>(&salvageHookEntry, defaultSalvageSessionSuccessPacketCallback);
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::SalvageSessionItemKept>(&salvageHookEntry, defaultSalvageSessionItemKeptPacketCallback);

  const auto bagsItems = getInventoryBagsItems();

  for (const auto &bagItem : bagsItems) {
    if (bagItem->model_id != 659) continue;

    const std::string modstring = getModstringFromItem(bagItem);
    if (doesModstringContainsRune(modstring)) salvageRune(bagItem);
  }

  for (const auto &bagItem : bagsItems) {
    if (bagItem->model_id != 659) continue;

    const std::string modstring = getModstringFromItem(bagItem);
    if (doesModstringContainsInsignia(modstring)) salvageInsignia(bagItem);
  }
}

void sellInventoryItemsToMerchant() {
  const auto bagsItems = getInventoryBagsItems();
  for (const auto &bagItem : bagsItems) {
    sellItemToMerchant(bagItem);
    Sleep(500);
  }
}

bool defaultCanStoreInChest(const GW::Item *item) {
  if (!item) return false;

  if (item->model_id == 22190) return true;  // shamrock ale
  if (item->model_id == 22191) return true;  // four leaf clover
  if (item->model_id == 22751) return true;  // lockpicks
  if (item->GetIsMaterial()) return true;
  if (item->model_id == 146 && (item->dye.dye_tint == 10 || item->dye.dye_tint == 12)) return true;  // black dye and white dye
  if (static_cast<GW::Constants::ItemType>(item->type) == GW::Constants::ItemType::Usable) return true;
  if (item->GetIsMaterial()) return true;

  return false;
}

void storeInventoryItemsInChest(const std::function<bool(const GW::Item *itemToStore)> &canStoreInChest) {
  GW::GameThread::Enqueue([]() { GW::Items::OpenXunlaiWindow(); });

  const auto bagsItems = getInventoryBagsItems();
  for (const auto &bagItem : bagsItems) {
    if (!canStoreInChest(bagItem)) continue;
    moveItemToStorage(bagItem);
    Sleep(500);
  }
}

uint16_t moveItemToStorage(const GW::Item *item, uint16_t quantity) {
  if (!item || !item->quantity) return 0;

  uint16_t to_move = std::min<uint16_t>(item->quantity, quantity);
  uint16_t remaining = to_move;

  if (item->GetIsMaterial() && remaining) remaining -= moveMaterialToStorageTab(item);

  const auto storage1 = static_cast<size_t>(GW::Constants::Bag::Storage_1);
  const auto storage14 = static_cast<size_t>(GW::Constants::Bag::Storage_14);

  if (remaining) remaining -= completeExistingItemStack(item, storage1, storage14, remaining);
  if (remaining) remaining -= moveItemToFirstEmptySlot(item, storage1, storage14, remaining);
  return to_move - remaining;
}

uint16_t moveItemToFirstEmptySlot(const GW::Item *item, size_t bag_first, size_t bag_last, uint16_t quantity) {
  quantity = std::min<uint16_t>(item->quantity, quantity);
  for (size_t bag_i = bag_first; bag_i <= bag_last; bag_i++) {
    const auto bag = GW::Items::GetBag(bag_i);
    if (!bag) continue;
    size_t slot = bag->find1(0);
    std::vector<uint32_t> pending_moves;
    while (slot != GW::Bag::npos) {
      if (!getPendingItemMove(bag_i, slot, pending_moves) && !bag->items[slot]) {
        setPendingMove(bag_i, slot, pending_moves);
        GW::Items::MoveItem(item, bag, slot, quantity);
        return quantity;
      }
      slot = bag->find1(0, slot + 1);
    }
  }
  return 0;
}

uint16_t moveMaterialToStorageTab(const GW::Item *item) {
  if (!item || !item->quantity) return 0;
  if (!item->GetIsMaterial()) return 0;

  int islot = GW::Items::GetMaterialSlot(item);
  if (islot < 0 || (int)GW::Constants::N_MATS <= islot) return 0;
  uint32_t slot = static_cast<uint32_t>(islot);
  const uint16_t max_in_slot = MaxMaterialStorage();
  uint16_t availaible = max_in_slot;
  GW::Item *b_item = GW::Items::GetItemBySlot(GW::Constants::Bag::Material_Storage, slot + 1);
  if (b_item) {
    if (b_item->quantity >= max_in_slot) return 0;
    availaible = max_in_slot - b_item->quantity;
  }
  uint16_t will_move = std::min<uint16_t>(item->quantity, availaible);
  if (will_move) GW::Items::MoveItem(item, GW::Constants::Bag::Material_Storage, slot, will_move);
  return will_move;
}

uint16_t MaxMaterialStorage() {
  uint16_t max_mat_storage_size = 250u;
  GW::Bag *bag = GW::Items::GetBag(GW::Constants::Bag::Material_Storage);
  if (!bag || bag->items.valid() || !bag->items_count) return max_mat_storage_size;
  GW::Item *b_item = nullptr;
  for (size_t i = GW::Constants::MaterialSlot::Bone; i < GW::Constants::MaterialSlot::N_MATS; i++) {
    b_item = bag->items[i];
    if (!b_item || b_item->quantity <= max_mat_storage_size) continue;
    while (b_item->quantity > max_mat_storage_size) {
      max_mat_storage_size += 250u;
    }
  }
  return max_mat_storage_size;
}

uint16_t completeExistingItemStack(const GW::Item *item, size_t bag_first, size_t bag_last, uint16_t quantity) {
  if (!item->GetIsStackable()) return 0;
  uint16_t to_move = std::min<uint16_t>(item->quantity, quantity);
  uint16_t remaining = to_move;
  for (size_t bag_i = bag_first; bag_i <= bag_last; bag_i++) {
    GW::Bag *bag = GW::Items::GetBag(bag_i);
    if (!bag) continue;
    size_t slot = bag->find2(item);
    std::vector<uint32_t> pending_moves;
    while (slot != GW::Bag::npos) {
      GW::Item *b_item = bag->items[slot];

      if (!getPendingItemMove(bag_i, slot, pending_moves) && b_item != nullptr && b_item->quantity < 250u) {
        uint16_t availaible = 250u - b_item->quantity;
        uint16_t will_move = std::min<uint16_t>(availaible, remaining);
        if (will_move > 0) {
          GW::Items::MoveItem(item, b_item, will_move);
          setPendingMove(bag_i, slot, pending_moves);
          remaining -= will_move;
        }
        if (remaining == 0) return to_move;
      }
      slot = bag->find2(item, slot + 1);
    }
    pending_moves.clear();
  }
  return to_move - remaining;
}

bool getPendingItemMove(uint32_t bag_idx, uint32_t slot, const std::vector<uint32_t> &pending_moves) {
  uint32_t bag_slot = (uint32_t)bag_idx << 16 | slot;
  return std::find(pending_moves.begin(), pending_moves.end(), bag_slot) != pending_moves.end();
}

void setPendingMove(uint32_t bag_idx, uint32_t slot, std::vector<uint32_t> &pending_moves) {
  uint32_t bag_slot = (uint32_t)bag_idx << 16 | slot;
  pending_moves.push_back(bag_slot);
}
