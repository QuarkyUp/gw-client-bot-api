#include <gwcba/agents.h>
#include <gwcba/movement.h>
#include <gwcba/utils.h>

bool isTimeElapsed(std::chrono::time_point<std::chrono::steady_clock> timePoint, uint32_t timeoutInMilliSeconds) {
  auto now = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::milliseconds>(now - timePoint).count() > timeoutInMilliSeconds;
}

bool move(GW::GamePos gamePos) {
  if (!arePointersValid()) return false;
  GW::GameThread::Enqueue([gamePos] {
    if (isPlayerLoadedOnMap()) GW::Agents::Move(gamePos.x, gamePos.y, gamePos.zplane);
  });

  return true;
}

bool moveTo(GW::GamePos destinationPos, const std::function<bool()> &moveStepCallback, uint32_t timeoutInMilliSeconds) {
  bool destinationReached = false;

  if (!arePointersValid()) return false;

  if (!move(destinationPos)) return false;

  auto start = std::chrono::high_resolution_clock::now();
  while (!destinationReached) {
    if (!moveStepCallback()) return false;

    const auto player = GW::Agents::GetPlayer();
    if (!player) return false;
    if (player->GetAsAgentLiving()->GetIsDead()) return false;

    if (!isAgentMoving(player))
      if (!move(destinationPos)) return false;

    Sleep(50);

    float distance = GW::GetSquareDistance(player ? player->pos : destinationPos, destinationPos);
    destinationReached = distance <= 25;

    if (isTimeElapsed(start, timeoutInMilliSeconds)) return false;
  }

  return destinationReached;
}

bool moveToMovingAgent(GW::Agent *movingAgent, const std::function<bool()> &moveStepCallback, uint32_t timeoutInMilliSeconds) {
  bool destinationReached = false;

  if (!arePointersValid()) return false;
  if (!movingAgent) return false;

  if (!move(movingAgent->pos)) return false;

  auto start = std::chrono::high_resolution_clock::now();
  while (!destinationReached) {
    if (!moveStepCallback()) return false;

    const auto player = GW::Agents::GetPlayer();
    if (!player) return false;
    if (player->GetAsAgentLiving()->GetIsDead()) return false;

    if (!movingAgent) return false;

    if (!isAgentMoving(player))
      if (!move(movingAgent->pos)) return false;

    Sleep(50);

    float distance = GW::GetSquareDistance(player ? player->pos : movingAgent->pos, movingAgent->pos);
    destinationReached = distance <= 150;

    if (isTimeElapsed(start, timeoutInMilliSeconds)) return false;
  }
  return true;
}

bool moveToZone(GW::GamePos destination, GW::Constants::MapID mapToLoad) {
  GW::Constants::MapID currentMapId = GW::Map::GetMapID();

  do {
    if (!arePointersValid()) return false;

    currentMapId = GW::Map::GetMapID();
    if (!move(destination)) return false;
    Sleep(50);
  } while (currentMapId != mapToLoad);

  return true;
}
