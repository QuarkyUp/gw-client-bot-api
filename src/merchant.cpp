#include <gwcba/agents.h>
#include <gwcba/inventory.h>
#include <gwcba/merchant.h>

std::vector<GW::ItemID> getItemsFromMerchant() {
  std::vector<GW::ItemID> merch_items;
  bool itemStreamParsed = false;

  GW::HookEntry merchantItemStreamEndEntry;
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::ItemStreamEnd>(&merchantItemStreamEndEntry,
                                                                    [&](GW::HookStatus *, GW::Packet::StoC::ItemStreamEnd *pak) -> void {
                                                                      // @Remark: unk1 = 13 means "selling" tab
                                                                      if (pak->unk1 != 12) return;
                                                                      GW::MerchItemArray &items = *GW::Merchant::GetMerchantItemsArray();
                                                                      merch_items.clear();
                                                                      for (size_t i = 0; i < items.size(); i++) merch_items.push_back(items[i]);
                                                                      itemStreamParsed = true;
                                                                    });

  GW::AgentLiving *merchant = getNearestAgentLivingByAllegianceToAgent(GW::Constants::Allegiance::Npc_Minipet);
  GW::Agents::GoNPC(merchant);

  while (!itemStreamParsed) Sleep(250);

  GW::StoC::RemoveCallback<GW::Packet::StoC::ItemStreamEnd>(&merchantItemStreamEndEntry);

  return merch_items;
}

GW::Item *getMerchantMaterial(Material material) {
  uint32_t model_id = getModelID(material);
  GW::ItemArray &items = *GW::Items::GetItemArray();
  std::vector<GW::ItemID> merch_items = getItemsFromMerchant();

  for (uint32_t item_id : merch_items) {
    if (item_id >= items.size()) continue;
    GW::Item *item = items[item_id];
    if (!item) continue;
    if (item->model_id == model_id) return item;
  }
  return nullptr;
}

Material getMaterial(uint32_t itemModelId) {
  switch (itemModelId) {
    case GW::Constants::ItemID::BoltofCloth:
      return BoltofCloth;
    case GW::Constants::ItemID::Bone:
      return Bone;
    case GW::Constants::ItemID::ChitinFragment:
      return ChitinFragment;
    case GW::Constants::ItemID::Feather:
      return Feather;
    case GW::Constants::ItemID::GraniteSlab:
      return GraniteSlab;
    case GW::Constants::ItemID::IronIngot:
      return IronIngot;
    case GW::Constants::ItemID::PileofGlitteringDust:
      return PileofGlitteringDust;
    case GW::Constants::ItemID::PlantFiber:
      return PlantFiber;
    case GW::Constants::ItemID::Scale:
      return Scale;
    case GW::Constants::ItemID::TannedHideSquare:
      return TannedHideSquare;
    case GW::Constants::ItemID::WoodPlank:
      return WoodPlank;
    case GW::Constants::ItemID::AmberChunk:
      return AmberChunk;
    case GW::Constants::ItemID::BoltofDamask:
      return BoltofDamask;
    case GW::Constants::ItemID::BoltofLinen:
      return BoltofLinen;
    case GW::Constants::ItemID::BoltofSilk:
      return BoltofSilk;
    case GW::Constants::ItemID::DeldrimorSteelIngot:
      return DeldrimorSteelIngot;
    case GW::Constants::ItemID::Diamond:
      return Diamond;
    case GW::Constants::ItemID::ElonianLeatherSquare:
      return ElonianLeatherSquare;
    case GW::Constants::ItemID::FurSquare:
      return FurSquare;
    case GW::Constants::ItemID::GlobofEctoplasm:
      return GlobofEctoplasm;
    case GW::Constants::ItemID::JadeiteShard:
      return JadeiteShard;
    case GW::Constants::ItemID::LeatherSquare:
      return LeatherSquare;
    case GW::Constants::ItemID::LumpofCharcoal:
      return LumpofCharcoal;
    case GW::Constants::ItemID::MonstrousClaw:
      return MonstrousClaw;
    case GW::Constants::ItemID::MonstrousEye:
      return MonstrousEye;
    case GW::Constants::ItemID::MonstrousFang:
      return MonstrousFang;
    case GW::Constants::ItemID::ObsidianShard:
      return ObsidianShard;
    case GW::Constants::ItemID::OnyxGemstone:
      return OnyxGemstone;
    case GW::Constants::ItemID::RollofParchment:
      return RollofParchment;
    case GW::Constants::ItemID::RollofVellum:
      return RollofVellum;
    case GW::Constants::ItemID::Ruby:
      return Ruby;
    case GW::Constants::ItemID::Sapphire:
      return Sapphire;
    case GW::Constants::ItemID::SpiritwoodPlank:
      return SpiritwoodPlank;
    case GW::Constants::ItemID::SteelIngot:
      return SteelIngot;
    case GW::Constants::ItemID::TemperedGlassVial:
      return TemperedGlassVial;
    case GW::Constants::ItemID::VialofInk:
      return VialofInk;
    default:
      return BoltofCloth;
  }
}

uint32_t getModelID(Material material) {
  switch (material) {
    case BoltofCloth:
      return GW::Constants::ItemID::BoltofCloth;
    case Bone:
      return GW::Constants::ItemID::Bone;
    case ChitinFragment:
      return GW::Constants::ItemID::ChitinFragment;
    case Feather:
      return GW::Constants::ItemID::Feather;
    case GraniteSlab:
      return GW::Constants::ItemID::GraniteSlab;
    case IronIngot:
      return GW::Constants::ItemID::IronIngot;
    case PileofGlitteringDust:
      return GW::Constants::ItemID::PileofGlitteringDust;
    case PlantFiber:
      return GW::Constants::ItemID::PlantFiber;
    case Scale:
      return GW::Constants::ItemID::Scale;
    case TannedHideSquare:
      return GW::Constants::ItemID::TannedHideSquare;
    case WoodPlank:
      return GW::Constants::ItemID::WoodPlank;
    case AmberChunk:
      return GW::Constants::ItemID::AmberChunk;
    case BoltofDamask:
      return GW::Constants::ItemID::BoltofDamask;
    case BoltofLinen:
      return GW::Constants::ItemID::BoltofLinen;
    case BoltofSilk:
      return GW::Constants::ItemID::BoltofSilk;
    case DeldrimorSteelIngot:
      return GW::Constants::ItemID::DeldrimorSteelIngot;
    case Diamond:
      return GW::Constants::ItemID::Diamond;
    case ElonianLeatherSquare:
      return GW::Constants::ItemID::ElonianLeatherSquare;
    case FurSquare:
      return GW::Constants::ItemID::FurSquare;
    case GlobofEctoplasm:
      return GW::Constants::ItemID::GlobofEctoplasm;
    case JadeiteShard:
      return GW::Constants::ItemID::JadeiteShard;
    case LeatherSquare:
      return GW::Constants::ItemID::LeatherSquare;
    case LumpofCharcoal:
      return GW::Constants::ItemID::LumpofCharcoal;
    case MonstrousClaw:
      return GW::Constants::ItemID::MonstrousClaw;
    case MonstrousEye:
      return GW::Constants::ItemID::MonstrousEye;
    case MonstrousFang:
      return GW::Constants::ItemID::MonstrousFang;
    case ObsidianShard:
      return GW::Constants::ItemID::ObsidianShard;
    case OnyxGemstone:
      return GW::Constants::ItemID::OnyxGemstone;
    case RollofParchment:
      return GW::Constants::ItemID::RollofParchment;
    case RollofVellum:
      return GW::Constants::ItemID::RollofVellum;
    case Ruby:
      return GW::Constants::ItemID::Ruby;
    case Sapphire:
      return GW::Constants::ItemID::Sapphire;
    case SpiritwoodPlank:
      return GW::Constants::ItemID::SpiritwoodPlank;
    case SteelIngot:
      return GW::Constants::ItemID::SteelIngot;
    case TemperedGlassVial:
      return GW::Constants::ItemID::TemperedGlassVial;
    case VialofInk:
      return GW::Constants::ItemID::VialofInk;
    default:
      return 0;
  }
}

uint32_t getMaterialPrice(Material material) {
  uint32_t materialPrice = 100 * 1000;
  bool waitForQuoteToEnd = true;

  GW::HookEntry quotedItemPriceEntry;
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::QuotedItemPrice>(&quotedItemPriceEntry,
                                                                      [&](GW::HookStatus *, GW::Packet::StoC::QuotedItemPrice *pak) -> void {
                                                                        materialPrice = pak->price;
                                                                        waitForQuoteToEnd = false;
                                                                      });

  GW::Item *item = getMerchantMaterial(material);
  if (!item) return 0;
  GW::Merchant::QuoteInfo give, recv;
  give.unknown = 0;
  give.item_count = 0;
  give.item_ids = nullptr;
  recv.unknown = 0;
  recv.item_count = 1;
  recv.item_ids = &item->item_id;
  GW::Merchant::RequestQuote(GW::Merchant::TransactionType::TraderBuy, give, recv);

  while (waitForQuoteToEnd) Sleep(250);

  GW::StoC::RemoveCallback<GW::Packet::StoC::QuotedItemPrice>(&quotedItemPriceEntry);

  return materialPrice;
}

uint32_t buyMaterial(Material material) {
  Transaction transaction(Transaction::Buy, material);
  bool waitForPurchaseEnd = true;

  GW::HookEntry quotedItemPriceEntry;
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::QuotedItemPrice>(
      &quotedItemPriceEntry, [&](GW::HookStatus *, GW::Packet::StoC::QuotedItemPrice *pak) -> void {
        if (transaction.type == Transaction::Quote) {
          printf("Quote\n");
          transaction.type = Transaction::Buy;
        } else if (transaction.type == Transaction::Buy) {
          auto gold_character = GW::Items::GetGoldAmountOnCharacter();
          if (gold_character >= pak->price) {
            GW::Merchant::TransactionInfo give, recv;
            give.item_count = 0;
            give.item_ids = nullptr;
            give.item_quantities = nullptr;
            recv.item_count = 1;
            recv.item_ids = &pak->itemid;
            recv.item_quantities = nullptr;

            GW::Merchant::TransactItems(GW::Merchant::TransactionType::TraderBuy, pak->price, give, 0, recv);
            waitForPurchaseEnd = true;
          }
        }
      });

  GW::HookEntry transactionDoneEntry;
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::TransactionDone>(&transactionDoneEntry,
                                                                      [&](GW::HookStatus *, GW::Packet::StoC::TransactionDone *pak) -> void {
                                                                        UNREFERENCED_PARAMETER(pak);
                                                                        waitForPurchaseEnd = false;
                                                                      });

  GW::Item *item = getMerchantMaterial(material);
  if (!item) return 0;
  GW::Merchant::QuoteInfo give, recv;
  give.unknown = 0;
  give.item_count = 0;
  give.item_ids = nullptr;
  recv.unknown = 0;
  recv.item_count = 1;
  recv.item_ids = &item->item_id;
  GW::Merchant::RequestQuote(GW::Merchant::TransactionType::TraderBuy, give, recv);

  while (waitForPurchaseEnd) Sleep(250);

  GW::StoC::RemoveCallback<GW::Packet::StoC::TransactionDone>(&transactionDoneEntry);
  GW::StoC::RemoveCallback<GW::Packet::StoC::QuotedItemPrice>(&quotedItemPriceEntry);

  return item->item_id;
}

void buyMaterial(Material material, uint32_t moneyLeft) {
  uint32_t totalMoney = GW::Items::GetGoldAmountInStorage() + GW::Items::GetGoldAmountOnCharacter();

  while (true) {
    totalMoney = GW::Items::GetGoldAmountInStorage() + GW::Items::GetGoldAmountOnCharacter();

    uint32_t materialPrice = getMaterialPrice(material);

    if (moneyLeft != 0) {
      if (totalMoney - materialPrice < moneyLeft) break;
    }

    if (GW::Items::GetGoldAmountOnCharacter() < materialPrice) {
      GW::Items::WithdrawGold(materialPrice);
      Sleep(1000);
    }
    if (GW::Items::GetGoldAmountOnCharacter() < materialPrice) {
      break;
    }

    buyMaterial(material);
  }
}

bool buyItemFromMerchant(uint32_t itemModelId, uint32_t itemPrice) {
  Sleep(500);
  GW::Item *kitToBuy = nullptr;
  GW::MerchItemArray *merchantArray = GW::Merchant::GetMerchantItemsArray();
  uint32_t size = merchantArray->m_capacity;
  for (uint32_t i = 0; i < size; i++) {
    GW::Item *merchantItem = GW::Items::GetItemById(**&(merchantArray->m_buffer) + i);
    if (!merchantItem) continue;
    if (merchantItem->model_id == itemModelId) {
      kitToBuy = merchantItem;
      break;
    }
  }

  if (!kitToBuy) return false;

  GW::Merchant::TransactionInfo give, recv;
  give.item_count = 0;
  give.item_ids = nullptr;
  give.item_quantities = nullptr;
  recv.item_count = 1;
  recv.item_ids = &kitToBuy->item_id;
  recv.item_quantities = nullptr;

  uint32_t availableSlotsCountBeforeBuy = getInventoryAvailableSlotsCount();

  GW::Merchant::TransactItems(GW::Merchant::TransactionType::MerchantBuy, itemPrice, give, 0, recv);
  Sleep(5000);

  uint32_t availableSlotsCountAfterBuy = getInventoryAvailableSlotsCount();

  return availableSlotsCountBeforeBuy != availableSlotsCountAfterBuy;
}

bool buySuperiorSalvageKit() { return buyItemFromMerchant(5900, 2000); }

bool buySuperiorIdentificationKit() {
  if (GW::Items::GetGoldAmountOnCharacter() + GW::Items::GetGoldAmountInStorage() < 500) return false;

  if (GW::Items::GetGoldAmountOnCharacter() >= 500) return buyItemFromMerchant(5899, 500);

  if (GW::Items::GetGoldAmountOnCharacter() < 500 && GW::Items::GetGoldAmountInStorage() >= 500) {
    GW::Items::WithdrawGold(500);
    return buyItemFromMerchant(5899, 500);
  }

  return false;
}

void buySalvageKitLogic() {
  if (!getSuperiorSalvageKitFromInventory()) {
    if (GW::Items::GetGoldAmountOnCharacter() >= 2000) {
      Sleep(1000);
      buySuperiorSalvageKit();
      Sleep(2500);
    } else if (GW::Items::GetGoldAmountInStorage() > 2000) {
      Sleep(1000);
      GW::Items::WithdrawGold(2000);
      Sleep(1000);
      buySuperiorSalvageKit();
      Sleep(2500);
    } else {
      return;
    }
  }
}

bool defaultExcludeFromSell(const GW::Item *item) {
  if (item->value <= 0) return true;
  if (item->type == static_cast<uint32_t>(GW::Constants::ItemType::Usable)) return true;
  if (item->type == static_cast<uint32_t>(GW::Constants::ItemType::Key)) return true;
  if (item->type == static_cast<uint32_t>(GW::Constants::ItemType::Kit)) return true;
  if (isItemARune(item)) return true;
  if (item->GetIsMaterial()) return true;

  if (item->model_id == 5899) return false;
  if (item->model_id == 5900) return false;

  return false;
}

void sellItemToMerchant(GW::Item *item, const std::function<bool(const GW::Item *itemToExcludeFromSell)> &excludeFromSell) {
  if (!item) return;
  if (excludeFromSell(item)) return;

  GW::Merchant::TransactionInfo give, recv;
  give.item_count = 1;
  give.item_ids = &item->item_id;
  give.item_quantities = nullptr;
  recv.item_count = 0;
  recv.item_ids = nullptr;
  recv.item_quantities = nullptr;

  GW::Merchant::TransactItems(GW::Merchant::TransactionType::MerchantSell, 0, give, item->quantity * item->value, recv);
  Sleep(750);
}

void defaultsellRuneQuotedPricePacketCallback([[maybe_unused]] GW::HookStatus *hookStatus, GW::Packet::StoC::QuotedItemPrice *pak) {
  GW::Merchant::TransactionInfo give, recv;
  give.item_count = 1;
  give.item_ids = &pak->itemid;
  give.item_quantities = nullptr;
  recv.item_count = 0;
  recv.item_ids = nullptr;
  recv.item_quantities = nullptr;

  if (pak->price + GW::Items::GetGoldAmountOnCharacter() >= 90000) {
    GW::Items::DepositGold();
    Sleep(1000);
  }

  GW::Merchant::TransactItems(GW::Merchant::TransactionType::TraderSell, 0, give, pak->price, recv);

  blockRuneSell = false;
}

bool sellRuneToTrader(GW::Item *item,
                      const std::function<void(GW::HookStatus *status, GW::Packet::StoC::QuotedItemPrice *pak)> &sellRuneQuotedPricePacketCallback) {
  GW::HookEntry runeSellHook;
  GW::StoC::RegisterPacketCallback<GW::Packet::StoC::QuotedItemPrice>(&runeSellHook, sellRuneQuotedPricePacketCallback);

  if (!isItemARune(item)) return false;

  GW::Merchant::QuoteInfo give, recv;
  give.unknown = 0;
  give.item_count = 1;
  give.item_ids = &item->item_id;
  recv.unknown = 0;
  recv.item_count = 0;
  recv.item_ids = nullptr;

  blockRuneSell = true;

  GW::Merchant::RequestQuote(GW::Merchant::TransactionType::TraderSell, give, recv);

  auto start = std::chrono::high_resolution_clock::now();
  while (true) {
    Sleep(50);
    if (!blockRuneSell) break;

    auto end = std::chrono::high_resolution_clock::now();
    auto timediffMs = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    if (timediffMs > 10000) {
      blockRuneSell = false;
      return false;
    }
  }

  return true;
}

uint32_t sellRunesToRuneTrader() {
  uint32_t runesSoldCount = 0;
  const auto inventoryBagsItems = getInventoryBagsItems();

  for (const auto &bagItem : inventoryBagsItems) {
    if (sellRuneToTrader(bagItem)) runesSoldCount++;
  }

  return runesSoldCount;
}
