#include <gwcba/utils.h>

bool arePointersValid() {
  if (GW::Agents::GetPlayer() == nullptr) return false;
  if (!GW::Map::GetIsMapLoaded()) return false;
  if (GW::Map::GetIsInCinematic()) return false;
  if (GW::Map::GetInstanceType() == GW::Constants::InstanceType::Loading) return false;

  return true;
}
