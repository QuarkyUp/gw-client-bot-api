#include <gwcba/agents.h>
#include <gwcba/utils.h>

bool isAgentMoving(const GW::Agent *agent) {
  if (!arePointersValid()) return false;

  return (agent->move_x != 0 && agent->move_y != 0);
}

bool isPlayerLoadedOnMap(GW::Constants::MapID MapId) {
  if (!GW::Map::GetIsMapLoaded()) return false;
  if (GW::Map::GetMapID() != MapId) return false;
  if (!GW::Agents::GetPlayer()) return false;
  return true;
}

bool areTherePlayersOnMap() {
  const auto &players = *GW::Agents::GetPlayerArray();
  if (!players.valid()) return false;
  if (players.size() > 1) return true;

  return false;
}

std::vector<GW::AgentLiving *> getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance allegiance, float range, const GW::Agent *agent) {
  std::vector<GW::AgentLiving *> agentsInRange = {};
  const auto &agents = *GW::Agents::GetAgentArray();

  if (!agents.valid()) return {};
  if (!arePointersValid()) return {};

  for (auto &lAgent : agents) {
    if (!arePointersValid()) continue;

    if (lAgent == nullptr) continue;

    GW::AgentLiving *agentLiving = lAgent->GetAsAgentLiving();
    if (agentLiving == nullptr) continue;
    if (!lAgent->GetIsLivingType()) continue;

    if (agentLiving->allegiance != allegiance) continue;
    if (agentLiving->GetIsDead()) continue;
    if (GW::GetSquareDistance(agent->pos, agentLiving->pos) >= range * range) continue;

    agentsInRange.push_back(agentLiving);
  }

  return agentsInRange;
}

std::vector<GW::AgentGadget *> getGadgetsInRangeOfAgent(float range, const GW::Agent *agent) {
  std::vector<GW::AgentGadget *> gadgetsInRange = {};
  const auto &agents = *GW::Agents::GetAgentArray();

  if (!agents.valid()) return {};
  if (!arePointersValid()) return {};

  for (auto &lAgent : agents) {
    if (!arePointersValid()) continue;

    if (lAgent == nullptr) continue;
    if (!lAgent->GetIsGadgetType()) continue;

    GW::AgentGadget *agentGadget = lAgent->GetAsAgentGadget();
    if (agentGadget == nullptr) continue;
    if (GW::GetSquareDistance(agent->pos, agentGadget->pos) >= range * range) continue;

    gadgetsInRange.push_back(agentGadget);
  }

  return gadgetsInRange;
}

std::vector<GW::AgentItem *> getAgentsItemInRangeOfAgent(float range, const GW::Agent *agent) {
  std::vector<GW::AgentItem *> agentsItemInRange = {};
  const auto &agents = *GW::Agents::GetAgentArray();

  if (!agents.valid()) return {};
  if (!arePointersValid()) return {};

  for (const auto &lAgent : agents) {
    if (!arePointersValid()) continue;

    if (lAgent == nullptr) continue;
    if (!lAgent->GetIsItemType()) continue;

    GW::AgentItem *agentItem = lAgent->GetAsAgentItem();
    if (agentItem == nullptr) continue;
    if (GW::GetDistance(agent->pos, lAgent->pos) >= range) continue;

    agentsItemInRange.push_back(agentItem);
  }

  return agentsItemInRange;
}

std::vector<GW::Item *> getItemsInRangeOfAgent(float range, const GW::Agent *agent) {
  std::vector<GW::Item *> itemsInRange = {};
  const auto &agents = *GW::Agents::GetAgentArray();

  if (!agents.valid()) return {};
  if (!arePointersValid()) return {};

  for (const auto &lAgent : agents) {
    if (!arePointersValid()) continue;

    if (!lAgent) continue;
    if (!lAgent->GetIsItemType()) continue;

    if (GW::GetDistance(agent->pos, lAgent->pos) >= range) continue;

    GW::Item *item = GW::Items::GetItemById(lAgent->GetAsAgentItem()->item_id);
    if (!item) continue;

    itemsInRange.push_back(item);
  }

  return itemsInRange;
}

std::vector<GW::AgentLiving *> getEnemiesInRangeOfAgent(float range, const GW::Agent *agent) {
  return getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance::Enemy, range, agent);
}

std::vector<GW::AgentLiving *> getAlliesInRangeOfAgent(float range, const GW::Agent *agent) {
  return getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance::Ally_NonAttackable, range, agent);
}

std::vector<GW::AgentLiving *> getNPCInRangeOfAgent(float range, const GW::Agent *agent) {
  return getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance::Npc_Minipet, range, agent);
}

std::vector<GW::AgentLiving *> getSpiritsInRangeOfAgent(float range, const GW::Agent *agent) {
  return getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance::Spirit_Pet, range, agent);
}

GW::AgentLiving *getNearestAgentLivingByAllegianceToAgent(GW::Constants::Allegiance allegiance, const GW::Agent *agent) {
  std::vector<GW::AgentLiving *> agentLivings = getAgentsLivingByAllegianceInRangeOfAgent(allegiance, GW::Constants::Range::Compass, agent);

  if (agentLivings.empty()) return nullptr;
  if (agentLivings.size() == 1) return agentLivings.at(0);

  GW::AgentLiving *nearestAgent = nullptr;

  float lastDistance = 10000.0f;
  for (const auto &agentLiving : agentLivings) {
    float distance = GW::GetDistance(agent->pos, agentLiving->pos);
    if (distance < lastDistance) {
      nearestAgent = agentLiving;
      lastDistance = distance;
    }
  }

  return nearestAgent;
}

GW::AgentGadget *getNearestGadgetAgentToAgent(const GW::Agent *agent) {
  const std::vector<GW::AgentGadget *> agentGadgets = getGadgetsInRangeOfAgent(GW::Constants::Range::Compass, agent);

  if (agentGadgets.empty()) return nullptr;
  if (agentGadgets.size() == 1) return agentGadgets.at(0);

  GW::AgentGadget *nearestGadget = nullptr;

  float lastDistance = 10000.0f;
  for (const auto &agentGadget : agentGadgets) {
    float distance = GW::GetDistance(agent->pos, agentGadget->pos);
    if (distance < lastDistance) {
      nearestGadget = agentGadget;
      lastDistance = distance;
    }
  }

  return nearestGadget;
}

GW::AgentItem *getNearestItemAgentToAgent(const GW::Agent *agent) {
  const std::vector<GW::AgentItem *> agentItems = getAgentsItemInRangeOfAgent(GW::Constants::Range::Compass, agent);

  if (agentItems.empty()) return nullptr;
  if (agentItems.size() == 1) return agentItems.at(0);

  GW::AgentItem *nearestAgentItem = nullptr;

  float lastDistance = 10000.0f;
  for (const auto &agentItem : agentItems) {
    float distance = GW::GetDistance(agent->pos, agentItem->pos);
    if (distance < lastDistance) {
      nearestAgentItem = agentItem;
      lastDistance = distance;
    }
  }

  return nearestAgentItem;
}

bool isItemARune(const GW::Item *item) {
  if (!arePointersValid()) return false;
  if (!item) return false;

  if (item->model_id == 15545 || item->model_id == 15546 || item->model_id == 15547 || item->model_id == 15548 || item->model_id == 15549 ||
      item->model_id == 15550 || item->model_id == 899 || item->model_id == 3612 || item->model_id == 5549 || item->model_id == 900 ||
      item->model_id == 5552 || item->model_id == 5553 || item->model_id == 901 || item->model_id == 5554 || item->model_id == 5555 ||
      item->model_id == 902 || item->model_id == 5556 || item->model_id == 5557 || item->model_id == 903 || item->model_id == 5558 ||
      item->model_id == 5559 || item->model_id == 904 || item->model_id == 5560 || item->model_id == 5561 || item->model_id == 5561 ||
      item->model_id == 6324 || item->model_id == 6325 || item->model_id == 6326 || item->model_id == 6327 || item->model_id == 6328 ||
      item->model_id == 6329 || item->model_id == 5550 || item->model_id == 5551 || item->model_id == 898 || item->model_id == 19124 ||
      item->model_id == 19125 || item->model_id == 19126 || item->model_id == 19127 || item->model_id == 19128 || item->model_id == 19129 ||
      item->model_id == 19130 || item->model_id == 19131 || item->model_id == 19132 || item->model_id == 19133 || item->model_id == 19134 ||
      item->model_id == 19135 || item->model_id == 19136 || item->model_id == 19137 || item->model_id == 19138 || item->model_id == 19139 ||
      item->model_id == 19140 || item->model_id == 19141 || item->model_id == 19142 || item->model_id == 19143 || item->model_id == 19144 ||
      item->model_id == 19145 || item->model_id == 19146 || item->model_id == 19147 || item->model_id == 19148 || item->model_id == 19149 ||
      item->model_id == 19150 || item->model_id == 19151 || item->model_id == 19152 || item->model_id == 19153 || item->model_id == 19154 ||
      item->model_id == 19155 || item->model_id == 19156 || item->model_id == 19157 || item->model_id == 19158 || item->model_id == 19159 ||
      item->model_id == 19160 || item->model_id == 19161 || item->model_id == 19162 || item->model_id == 19163 || item->model_id == 19164 ||
      item->model_id == 19165 || item->model_id == 19166 || item->model_id == 19167 || item->model_id == 19168)
    return true;
  return false;
}

GW::Player *getPartyLeader() {
  auto g = GW::GameContext::instance();
  if (!g) return nullptr;
  auto c = g->party;
  if (!c) return nullptr;

  GW::PartyInfo *partyInfo = c->player_party;
  if (!partyInfo) return nullptr;
  if (!(partyInfo->players.valid() && partyInfo->players.size())) return nullptr;

  return GW::PlayerMgr::GetPlayerByID(partyInfo->players[0].login_number);
}