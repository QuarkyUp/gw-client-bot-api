#include <gwcba/agents.h>
#include <gwcba/movement.h>
#include <gwcba/skillbar.h>
#include <gwcba/utils.h>

void loadBuildTemplate(const std::string &buildTemplate) {
  if (GW::Map::GetInstanceType() != GW::Constants::InstanceType::Outpost) return;
  const char *codeChar = buildTemplate.c_str();
  GW::SkillbarMgr::LoadSkillTemplate(codeChar, 0);
}

bool useSkill(uint32_t skillSlot, GW::AgentID targetAgentId, uint32_t timeoutInMilliSeconds) {
  if (!arePointersValid()) return false;

  if (skillSlot < 1 || skillSlot > 8) return false;
  if (!GW::Agents::GetAgentByID(targetAgentId)) return false;

  const auto player = GW::Agents::GetPlayer()->GetAsAgentLiving();
  if (player->GetAsAgentLiving()->GetIsDead()) return false;

  const auto startUseSkillTimer = std::chrono::high_resolution_clock::now();

  const auto skillbar = GW::SkillbarMgr::GetPlayerSkillbar();
  if (!skillbar || !skillbar->IsValid()) return false;
  if (skillbar->skills[skillSlot - 1].GetRecharge() != 0) return false;

  do {
    if (!arePointersValid()) return false;
    Sleep(150);
  } while (isPlayerLoadedOnMap() && player->GetAsAgentLiving()->GetIsCasting());

  GW::GameThread::Enqueue([skillSlot, targetAgentId]() { GW::SkillbarMgr::UseSkill(skillSlot - 1, targetAgentId); });

  const auto &skill = skillbar->skills[skillSlot - 1];
  const auto &skilldata = *GW::SkillbarMgr::GetSkillConstantData(skill.skill_id);

  if (skilldata.activation != 0) {
    float skillCastDelay = max(skilldata.activation + skilldata.aftercast, 0.25f);
    Sleep(static_cast<DWORD>(skillCastDelay * 1000));
  } else {
    float skillCastDelay = max(skilldata.activation + skilldata.aftercast, 0.15f);
    Sleep(static_cast<DWORD>(skillCastDelay * 1000));
  }

  while (skillbar->skills[skillSlot].GetRecharge() == 0 && player->GetIsCasting() && player->skill != 0) {
    Sleep(50);
    if (!arePointersValid()) return false;
    if (player->GetIsDead()) return false;
    if (isTimeElapsed(startUseSkillTimer, timeoutInMilliSeconds)) return false;
  }

  return true;
}

std::vector<GW::Effect> getEffectsOnPlayer() {
  std::vector<GW::Effect> effects = {};

  const GW::AgentEffectsArray *agent_effects_array = GW::Effects::GetPartyEffectsArray();
  if (agent_effects_array == nullptr) return effects;

  for (auto &agent_effects_it : *agent_effects_array) {
    auto &agent_effects = agent_effects_it.effects;
    if (!agent_effects.valid()) continue;
    const auto agent_id = agent_effects_it.agent_id;

    const auto player = GW::Agents::GetPlayer();
    if (player->agent_id == agent_id) {
      for (const GW::Effect &effect : agent_effects) {
        effects.push_back(effect);
      }
      break;
    }
  }

  return effects;
}

bool isPlayerUnderSkillEffect(GW::Constants::SkillID skillId) {
  if (!arePointersValid()) return false;

  for (const auto &effect : getEffectsOnPlayer())
    if (static_cast<GW::Constants::SkillID>(effect.skill_id) == skillId) return true;

  return false;
}

uint32_t getEffectTimeRemainingOnPlayer(GW::Constants::SkillID skillId) {
  auto playerEffects = getEffectsOnPlayer();

  for (const auto &effect : playerEffects) {
    if (static_cast<GW::Constants::SkillID>(effect.skill_id) == skillId) {
      return effect.GetTimeRemaining();
    }
  }
  return 0;
}

bool isRecharged(uint32_t skillSlot) {
  if (!arePointersValid()) return false;
  const auto skillbar = GW::SkillbarMgr::GetPlayerSkillbar();
  if (!skillbar || !skillbar->IsValid()) return false;

  return skillbar->skills[skillSlot - 1].GetRecharge() <= 0;
}
