#include <gwcba/agents.h>
#include <gwcba/bot.h>
bool botRunning = false;

void runLogicOnMapLoading(const std::map<GW::Constants::MapID, const std::function<bool(void)>>& logicOnMap) {
  while (botRunning) {
    for (auto& [mapId, logic] : logicOnMap) {
      if (!isPlayerLoadedOnMap(mapId)) continue;
      if (GW::Map::GetMapID() == mapId) logic();
    }
  }
}
