#include <gwcba/faction.h>
#include <gwcba/utils.h>

bool takeFactionBlessing() {
  if (!arePointersValid()) return false;

  GW::WorldContext* ctx = GW::GameContext::instance()->world;
  GW::Agent* player = GW::Agents::GetPlayer();

  if (ctx->current_kurzick > ctx->current_luxon) {
    GW::Agents::SendDialog(0x85);
    GW::Agents::SendDialog(0x86);
  }

  if (ctx->current_kurzick < ctx->current_luxon) {
    GW::Agents::SendDialog(0x81);
    GW::Agents::SendDialog(0x2);
    GW::Agents::SendDialog(0x84);
    GW::Agents::SendDialog(0x86);
  }

  if (ctx->current_kurzick == ctx->current_luxon) {
    if (!arePointersValid()) return false;

    const auto& guilds = *GW::GuildMgr::GetGuildArray();
    if (!guilds.valid()) return false;
    if (player->GetAsAgentLiving()->tags->guild_id >= guilds.size()) return false;

    const auto guild = guilds[player->GetAsAgentLiving()->tags->guild_id];
    if (!guild) return false;

    // luxon guild
    if (guild->faction) {
      GW::Agents::SendDialog(0x81);
      GW::Agents::SendDialog(0x2);
      GW::Agents::SendDialog(0x84);
      GW::Agents::SendDialog(0x86);
    }
    // kurzick guild
    else {
      GW::Agents::SendDialog(0x85);
      GW::Agents::SendDialog(0x86);
    }
  }

  return true;
}
