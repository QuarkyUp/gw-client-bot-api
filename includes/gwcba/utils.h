#ifndef GWCBA_UTILS_H
#define GWCBA_UTILS_H

#include <gwcba/gwcba.h>

bool arePointersValid();

#endif  // GWCBA_UTILS_H
