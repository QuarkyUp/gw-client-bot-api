#ifndef GWCBA_GWCBA_H
#define GWCBA_GWCBA_H

#pragma warning(disable: 4201) // nonstandard extension used : nameless struct/union
#pragma warning(disable: 4505) // 'function' : unreferenced local function has been removed

#include <Windows.h>
#include <iostream>
#include <functional>
#include <chrono>
#include <thread>
#include <sstream>
#include <memory>
#include <algorithm>
#include <vector>
#include <algorithm>
#include <iostream>

#include <GWCA/GameContainers/Array.h>
#include <GWCA/GameContainers/List.h>
#include <GWCA/Packets/Opcodes.h>
#include <GWCA/Packets/StoC.h>

#include <GWCA/Constants/Constants.h>

#include <GWCA/GameEntities/Agent.h>
#include <GWCA/GameEntities/Player.h>
#include <GWCA/GameEntities/Item.h>
#include <GWCA/GameEntities/Skill.h>
#include <GWCA/GameEntities/Guild.h>
#include <GWCA/GameEntities/Attribute.h>
#include <GWCA/GameEntities/Party.h>

#include <GWCA/Context/ItemContext.h>
#include <GWCA/Context/WorldContext.h>
#include <GWCA/Context/GameContext.h>
#include <GWCA/Context/PartyContext.h>

#include <GWCA/Managers/AgentMgr.h>
#include <GWCA/Managers/GameThreadMgr.h>
#include <GWCA/Managers/SkillbarMgr.h>
#include <GWCA/Managers/MapMgr.h>
#include <GWCA/Managers/PlayerMgr.h>
#include <GWCA/Managers/ItemMgr.h>
#include <GWCA/Managers/MerchantMgr.h>
#include <GWCA/Managers/PartyMgr.h>
#include <GWCA/Managers/CtoSMgr.h>
#include <GWCA/Managers/UIMgr.h>
#include <GWCA/Managers/GuildMgr.h>
#include <GWCA/Managers/ChatMgr.h>
#include <GWCA/Managers/SkillbarMgr.h>
#include <GWCA/Managers/StoCMgr.h>
#include <GWCA/Managers/EffectMgr.h>

#endif //GWCBA_GWCBA_H
