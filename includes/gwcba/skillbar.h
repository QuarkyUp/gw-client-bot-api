#ifndef GWCBA_SKILLBAR_H
#define GWCBA_SKILLBAR_H

#include <gwcba/gwcba.h>

void loadBuildTemplate(const std::string& buildTemplate);
bool useSkill(uint32_t skillSlot, GW::AgentID targetAgentId, uint32_t timeoutInMilliSeconds = 3000);

std::vector<GW::Effect> getEffectsOnPlayer();
bool isPlayerUnderSkillEffect(GW::Constants::SkillID skillId);
uint32_t getEffectTimeRemainingOnPlayer(GW::Constants::SkillID skillId);
bool isRecharged(uint32_t skillSlot);

#endif  // GWCBA_SKILLBAR_H
