#ifndef GWCBA_TRAVEL_H
#define GWCBA_TRAVEL_H

#include <gwcba/gwcba.h>

void mapTravelRandom(GW::Constants::MapID mapId, bool checkPlayerOnMap = false);
void mapTravel(GW::Constants::MapID mapId, GW::Constants::District district, uint32_t districtNumber = 0);
int regionFromDistrict(GW::Constants::District _district);
int languageFromDistrict(GW::Constants::District _district);
void waitForMapLoaded(GW::Constants::MapID mapId, uint32_t timeoutInMilliSeconds = 10000);
bool waitForCurrentMapLoaded(uint32_t timeoutInMilliSeconds = 10000);

#endif  // GWCBA_TRAVEL_H
