#ifndef GWCBA_BOT_H
#define GWCBA_BOT_H

#include <gwcba/gwcba.h>

#include <map>

void runLogicOnMapLoading(const std::map<GW::Constants::MapID, const std::function<bool(void)>> &logicOnMap);

extern bool botRunning;
#endif  // GWCBA_BOT_H
