#ifndef GWCBA_AGENTS_H
#define GWCBA_AGENTS_H

#include <gwcba/gwcba.h>

bool isAgentMoving(const GW::Agent* agent);
bool isPlayerLoadedOnMap(GW::Constants::MapID MapId = GW::Map::GetMapID());
bool areTherePlayersOnMap();

std::vector<GW::AgentLiving*> getAgentsLivingByAllegianceInRangeOfAgent(GW::Constants::Allegiance allegiance,
                                                                        float range = GW::Constants::Range::Spellcast,
                                                                        const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentGadget*> getGadgetsInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentItem*> getAgentsItemInRangeOfAgent(float range = GW::Constants::Range::Spellcast,
                                                        const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::Item*> getItemsInRangeOfAgent(float range = GW::Constants::Range::Spellcast, const GW::Agent* agent = GW::Agents::GetPlayer());

std::vector<GW::AgentLiving*> getEnemiesInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> getAlliesInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> getNPCInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> getSpiritsInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());

GW::AgentLiving* getNearestAgentLivingByAllegianceToAgent(GW::Constants::Allegiance allegiance, const GW::Agent* agent = GW::Agents::GetPlayer());
GW::AgentGadget* getNearestGadgetAgentToAgent(const GW::Agent* agent = GW::Agents::GetPlayer());
GW::AgentItem* getNearestItemAgentToAgent(const GW::Agent* agent = GW::Agents::GetPlayer());

bool isItemARune(const GW::Item* item);

GW::Player* getPartyLeader();

#endif  // GWCBA_AGENTS_H
