#ifndef GWCBA_MOVEMENT_H
#define GWCBA_MOVEMENT_H

#include <gwcba/gwcba.h>

bool isTimeElapsed(std::chrono::time_point<std::chrono::steady_clock> timePoint, uint32_t timeoutInMilliSeconds);

bool move(GW::GamePos gamePos);
bool moveTo(
    GW::GamePos gamePos, const std::function<bool()>& moveStepCallback = [] { return true; }, uint32_t timeoutInMilliSeconds = 60000);
bool moveToMovingAgent(
    GW::Agent* movingAgent, const std::function<bool()>& moveStepCallback = []() { return true; }, uint32_t timeoutInMilliSeconds = 60000);

bool moveToZone(GW::GamePos destination, GW::Constants::MapID mapToLoad);

#endif  // GWCBA_MOVEMENT_H
