#ifndef GWCBA_INVENTORY_H
#define GWCBA_INVENTORY_H

#include <gwcba/gwcba.h>

std::vector<GW::Item*> getInventoryBagsItems();
uint32_t getInventoryTotalSlotsCount();
uint32_t getInventoryAvailableSlotsCount();

const GW::Item* getItemFromInventory(uint32_t itemModelId);
const GW::Item* getSuperiorIdentificationKitFromInventory();
const GW::Item* getSuperiorSalvageKitFromInventory();

bool defaultCanPickup(const GW::Item* item);
bool pickupItem(const GW::Item* item, const std::function<bool(const GW::Item* itemToPickUp)>& canPickup = defaultCanPickup);
bool pickupItemsInRangeOfPlayer(float range = GW::Constants::SqrRange::Spellcast,
                                const std::function<bool(const GW::Item* itemToPickUp)>& canPickup = defaultCanPickup);

bool identify(const GW::Item* item, const GW::Item* kit);
bool identifyInventoryItems();

void salvageMod(const GW::Item* item, uint32_t modType);
void salvageRune(const GW::Item* item);
void salvageInsignia(const GW::Item* item);
void salvageModsFromInventoryItems();
void sellInventoryItemsToMerchant();
bool defaultCanStoreInChest(const GW::Item* item);
void storeInventoryItemsInChest(const std::function<bool(const GW::Item* itemToStore)>& canStoreInChest = defaultCanStoreInChest);

bool canStoreItemInChest(const GW::Item* item);
bool canPickUpItem(const GW::Item* item);

// Credits to GWToolbox
uint16_t moveItemToStorage(const GW::Item* item, uint16_t quantity = 1000u);
uint16_t moveItemToFirstEmptySlot(const GW::Item* item, size_t bag_first, size_t bag_last, uint16_t quantity = 1000u);
uint16_t moveMaterialToStorageTab(const GW::Item* item);
uint16_t MaxMaterialStorage();
uint16_t completeExistingItemStack(const GW::Item* item, size_t bag_first, size_t bag_last, uint16_t quantity = 1000u);
bool getPendingItemMove(uint32_t bag_idx, uint32_t slot, const std::vector<uint32_t>& pending_moves);
void setPendingMove(uint32_t bag_idx, uint32_t slot, std::vector<uint32_t>& pending_moves);

extern bool blockBeforeSalvage;
extern bool blockAfterSalvage;
extern bool blockRuneSell;
#endif  // GWCBA_INVENTORY_H
