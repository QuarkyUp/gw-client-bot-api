#ifndef GWCBA_MERCHANT_H
#define GWCBA_MERCHANT_H

#include <gwcba/gwcba.h>

enum Material {
  BoltofCloth,
  Bone,
  ChitinFragment,
  Feather,
  GraniteSlab,
  IronIngot,
  PileofGlitteringDust,
  PlantFiber,
  Scale,
  TannedHideSquare,
  WoodPlank,

  AmberChunk,
  BoltofDamask,
  BoltofLinen,
  BoltofSilk,
  DeldrimorSteelIngot,
  Diamond,
  ElonianLeatherSquare,
  FurSquare,
  GlobofEctoplasm,
  JadeiteShard,
  LeatherSquare,
  LumpofCharcoal,
  MonstrousClaw,
  MonstrousEye,
  MonstrousFang,
  ObsidianShard,
  OnyxGemstone,
  RollofParchment,
  RollofVellum,
  Ruby,
  Sapphire,
  SpiritwoodPlank,
  SteelIngot,
  TemperedGlassVial,
  VialofInk,

  N_MATS
};

struct Transaction {
  enum Type { Sell, Buy, Quote };
  Type type;
  uint32_t item_id;
  Material material;

  Transaction(Type t, Material mat) : type(t), item_id(0), material(mat) {}
};

std::vector<GW::ItemID> getItemsFromMerchant();
GW::Item* getMerchantMaterial(Material material);
Material getMaterial(uint32_t itemModelId);
uint32_t getModelID(Material material);

uint32_t getMaterialPrice(Material material);
uint32_t buyMaterial(Material material);
void buyMaterial(Material material, uint32_t moneyLeft);

bool buyItemFromMerchant(uint32_t itemModelId, uint32_t itemPrice);
bool buySuperiorSalvageKit();
bool buySuperiorIdentificationKit();
void buySalvageKitLogic();

bool defaultExcludeFromSell(const GW::Item* item);
void sellItemToMerchant(GW::Item* item, const std::function<bool(const GW::Item* itemToExcludeFromSell)>& excludeFromSell = defaultExcludeFromSell);

void defaultsellRuneQuotedPricePacketCallback(GW::HookStatus* hookStatus, GW::Packet::StoC::QuotedItemPrice* pak);
bool sellRuneToTrader(GW::Item* item,
                      const std::function<void(GW::HookStatus* status, GW::Packet::StoC::QuotedItemPrice* pak)>& sellRuneQuotedPricePacketCallback =
                          defaultsellRuneQuotedPricePacketCallback);
uint32_t sellRunesToRuneTrader();

#endif  // GWCBA_MERCHANT_H
