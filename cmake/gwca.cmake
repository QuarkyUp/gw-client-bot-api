include_guard()
include(FetchContent)

FetchContent_Declare(
        gwca
        GIT_REPOSITORY https://github.com/GregLando113/GWCA.git
        GIT_TAG ddde29f4a292813867240ff297f20521aecb9e47
)
FetchContent_MakeAvailable(gwca)

